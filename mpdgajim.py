"""
 MPD2Gajim 0.1.1.1
 MPD to Gajim (via DBUS) script.
 
 Copyright (c) 2007, Alexander Stanley
 alexanderwstanley@gmail.com
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without modification, are permitted 
 provided that the following conditions are met:
 
 * Redistributions of source code must retain the above copyright notice, this list of conditions 
 and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list of 
 conditions and the following disclaimer in the documentation and/or other materials provided with the 
 distribution.
  * Neither the name of the Orloglausa.Net nor the names of its contributors may be used to endorse 
 or promote products derived from this software without specific prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 """
 ### Only play with this if your system is getting
 ### flogged by the process (which is shouldn't be)
 interval = 1       # number of seconds between checks
 
 import dbus
 import sys
 import string
 import os
 import cgi
 import time
 import mpdclient2
 import datetime
 
 old_t = ""
 old_a = ""
 old_n = ""
 tmp_a = ""
 tmp_t = ""
 tmp_n = ""
 tichk = 0
 pause = 0
 
 bus = dbus.SessionBus()
 obj = bus.get_object('org.gajim.dbus', '/org/gajim/dbus/RemoteObject')
 myinterface = dbus.Interface(obj, 'org.gajim.dbus.RemoteInterface')
 
 def update(title, artist, track, album, mytime):
     global pause
     note = u'\u266B'
     pausestr = ""
     if pause == 1:
        pausestr = "[Paused]"
        tichk = 0
     stat_msg = str("%s %s - %s %s %s") % (note,artist,title,note,pausestr)
     global myinterface
     for account in myinterface.list_accounts():
         status = myinterface.get_status(account)
         myinterface.change_status(status,stat_msg,account)
 
 def stopmusic():
     global tmp_a
     global tmp_n
     global tmp_t
     global tichk
     stat_msg = str("Music Stopped.")
     global myinterface
     for account in myinterface.list_accounts():
         status = myinterface.get_status(account)
         myinterface.change_status(status,stat_msg,account)
     tmp_a = ""
     tmp_t = ""
     tmp_n = ""
     tichk = 0
 
 def checksong():
   m = mpdclient2.connect()
   if str(m.status().state) == str("stop"):
     stopmusic()
   else:
     global pause
     global old_t
     global old_n
     global old_a
     global tmp_t
     global tmp_n
     global tmp_a
     global tichk
     var = m.currentsong()
     s = m.status()
     if 'album' in var:
       album = str(var['album'])
     else: 
       album = " "
     if 'artist' in var:
       artist = str(var['artist'])
     else: 
       artist = " "
     if 'title' in var:
       title = str(var['title'])
     else: 
       title = " "
     if 'track' in var:
       track = str(var['track'])
     else: 
       track = int(0)
     current = s.time.split(':', 2)[0]
     mytime = s.time.split(':', 2)[1]
     mytime = str("[" + str(timehack(mytime)+ "]"))
     tichk = tichk + 1
 
     if str(m.status().state) == str("pause"):
       pause = 1
       update(title,artist,track,album,mytime)
     else:
       pause = 0
       if current > 5:
         if str(old_t) == str(title) and str(old_a) == str(artist):
           if tichk > int(current):
             update(title, artist, track, album, mytime)
         else:
             update(title, artist, track, album, mytime)
             tmp_t = title
             tmp_a = artist
       else:
             tichk == current - 3
  
     if (str(tmp_t) != str(title) and str(old_a) != str(artist)):
        old_a = tmp_a
        old_t = tmp_t
 
 def timehack(vartime):
     valmins = int(vartime)/60
     valsecs = int(vartime) - (valmins * 60)
     if valsecs < 10:
        valsecs = "%s%s" % (0,valsecs)
     valtime = "%s:%s" % (valmins,valsecs)
     return valtime
 
 def main():
     while 1:
         checksong()
         time.sleep(interval) 
     main()
 
 def createDaemon():
     try:
         if os.fork() > 0: os._exit(0)
     except OSError, error:
         print 'Error in fork 1 :: %d (%s)' % (error.errno, error.strerror)
         os._exit(1)
 
     os.chdir('/')
     os.setsid()
     os.umask(0)
 
     try:
         pid = os.fork()
         if pid > 0:
             print 'Daemonised (PID: %d)' % pid
             os._exit(0)
     except OSError, error:
         print 'Error in fork 2 :: %d (%s)' % (error.errno, error.strerror)
         os._exit(1)
 
     main()
 
 createDaemon()
