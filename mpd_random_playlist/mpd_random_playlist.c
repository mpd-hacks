/*
 *  mpd_random_playlist.c                                 19.01.2009
 *
 *  Copyright 2009 Max Christian Pohle <webmaster@coderonline.de>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *  MA 02110-1301, USA.
 */
#define PROGRAM_NAME "mpd_random_playlist"

#include <stdio.h>
#include <stdlib.h>
#include <libmpd/libmpd.h>
#include <string.h>
#include <time.h>

void help(char *program);
void error_callback(MpdObj *mi,int errorid, char *msg, void *userdata);

int main(int argc, char **argv)
{
  int iport  = 6600;
  int icount = 50;
  int silent = 1;

  char *hostname = getenv("MPD_HOST");
  char *port     = getenv("MPD_PORT");
  char *password = getenv("MPD_PASSWORD");
  char *count    = argv[1];


  if(!hostname) { hostname = "localhost"; }
  if(!password) { password = ""; }
  if(count)     { icount = atoi(count); }
  if(port)      { iport  = atoi(port); }

  if(!silent)
    printf("==== " PROGRAM_NAME "====\n\n");
  if(icount <= 0)
  {
    help(PROGRAM_NAME);
    exit(0);
  }

  MpdObj *mo = mpd_new(hostname, iport, password);

  if(!mpd_connect(mo))
  { mpd_send_password(mo); }

  mpd_signal_connect_error(mo, (ErrorCallback)error_callback, NULL);

  if(!silent)
    printf("STATUS\n");
  if (!mpd_check_connected(mo))
  {
    mpd_free(mo);
    printf("\tERROR: failed to connect to %s\n", hostname);
    return 1;
  }
  else
  {
    if(!silent)
      printf("\t> connection to host `%s` estamblished.\n", hostname);
  }

  MpdData *listItems = mpd_database_get_complete(mo);

  int total_songs    = mpd_stats_get_total_songs(mo);
  int total_playtime = mpd_stats_get_db_playtime(mo);
  if(!silent)
  {
    printf("\t> found %d songs in database.\n", total_songs);
    printf("\t> with a total playtime of %d\n", total_playtime);
    printf("\t> random selection of %d files starts now...\n", icount);
  }
  srand( (unsigned)time( NULL ) );
  int i;
  for(i=1;i<=icount; i++)
  {
    float random_song = ((float) rand() / RAND_MAX) * total_songs;
    int songNr        = (int) random_song;

    int q;
    for(q=0; q<=songNr; q++)
    {
      listItems = mpd_data_get_next(listItems);
      if (mpd_data_is_last(listItems))
      { listItems = mpd_data_get_first(listItems); }

      if(listItems->type != MPD_DATA_TYPE_SONG)
      { q--; }
    }

    if(!silent)
      printf("\t\t> adding %s\n", listItems->song->title);
    mpd_playlist_queue_add(mo, listItems->song->file);
  }
  if(!silent)
    printf("\t> random selection successfully done.\n");

  mpd_playlist_clear(mo);
  if(!silent)
    printf("\t> cleared playlist.\n");
  mpd_playlist_queue_commit(mo);
  if(!silent)
    printf("\t> submitted new playlist with random songs.\n");
  mpd_player_play(mo);
  if(!silent)
    printf("\t> started playback.\n\n");


  mpd_free(mo);
  exit(0);
}

void error_callback(MpdObj *mi,int errorid, char *msg, void *userdata)
{
  help(PROGRAM_NAME);
  printf("ERROR: %s\n...maybe wrong password?", msg);
  exit(1);
}

void help(char *program)
{
  printf(
    "HELP\n"
    "\tPROGRAM-CALL\n"
    "\t./%s [--help] [<NUMBER_OF_RANDOM_FILES>]\n\n"
    "\tENVIROMENT-VARIABLES\n\n"
    "\tMPD_HOST\thostname of mpd (optional)\n"
    "\tMPD_PORT\tport to connect with mpd (optional)\n"
    "\tMPD_PASSWORD\tpassword for the mpd-server (optional, but recommend)\n\n"
    "\t...to set these use export <VARIABLE>=<VALUE>-syntax\n\n"
    , program
  );
}
