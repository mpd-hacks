############################################################################
# Commands:
# /mplay [title|filename|artist|album] <search arg>
# First arguments are optional, if left out, title is used.
#
# /madd [title|filename|artist|album] <search arg>
# /mremove [title|filename|artist|album] <search arg>
# /mload [title|filename|artist|album] <search arg>
# First arguments are optional, if left out, artist is used.
# If invoked without any arguments all files are added/removed to/from list.
#
# /madd will search for <search args> and add result to current playlist
# /mremove will search for <search args> and remove matches from current playlist
# /mload will search for <search args> and create a playlist  with the found files. 
# So if you want to listen to the artist FooBars you type /mload artist FooBars
#
# /mshow
# Prints Artist - Album - Title to active window
# Not that anyone will ever be interested in what song you are 
# listening to, but what the heck.
#
# (Self explained)
# /mnext
# /mprev
# /mseek <percent>
# /mpause
# /mstop
# /mrandom [on/1|off/0]
# /mrepeat [on/1|off/0]
# /mvolume [percent]
# /mclear
# /mconnect
#
# Variables (settings) - set from irssi with /set
#
# mpd_show_state	ON/OFF
#   > - play
#  || - pause
#  [] - stop
#   ? - wha?
# mpd_show_artist	ON/OFF
# mpd_show_album	ON/OFF
# mpd_show_title	ON/OFF
# mpd_show_time		ON/OFF
# mpd_show_percent	ON/OFF
# mpd_show_volume	ON/OFF
# mpd_show_mode		ON/OFF
#  r - repeat
#  z - random
# mpd_autoreconnect	ON/OFF
#
#
# Enjoy!
#
# Known bugs:
#
# Changelog:
#
# 2.1.3 - Corrected Audio::MPD constructor arguments
# 2.1.2 - Added /mremove
# 2.1.1 - Added mpd_show_volume and mpd_show_mode.
#         Changed position-format from ##.#% to ##%
#         Removed iconv-stuff
# 2.1.0 - Rewritten for Audio::MPD 0.19.1
#         Added mpd_show_artist, mpd_show_album
# 2.0.5 - Removed some redudant code.
#         Below only works with Audio::MPD 0.12.4 patched with Audio-MPD-0.12.4-safeconnect.patch
#         (http://0xa0.org/stuff/Audio-MPD-0.12.4-safeconnect.patch)
#         Added setting: mpd_autoreconnect
#         Added /mconnect
# 2.0.4 - MPD seems to have moved back to Audio::MPD in 0.12.3 ...
#         I'll leave it to the user to change these two lines of code
#         back to MPD as in 2.0.3 if they happen to be using an older version
#         of Audio::MPD (MPD).
# 2.0.3 - Audio::MPD seems to have moved to MPD, appropriate changes made.
# 2.0.2 - Missing ;
# 2.0.1 - Fixed a bug in /mplay where no results were played unless the
#         first match were a track in the playlist.
# 2.0.0	- Total rewrite. Now uses Audio::MPD. Most things remain the same.
#         /mremove was removed since it was very ugly and I found no nice way
#         to implement it.
#         mpd_show_artist was removed and is now a part of mpd_show_title.
# 1.3.4 - Just changed the hardcoded MPD_PORT and MPD_HOST to use the env-vars
#         if set.
# 1.3.3 - Removed $data="OK" from get_playlistinfo since it caused ogg/flac 
#         files titles to not be displayed, as pointed out by jat in #mpd.
# 1.3.2 - Added utf8 to iso-8859-15 convertion. No more ö here!
# 1.3.1 - if no tag, display filename, as pointed out by t0ffel. Thanks.
#	  added /minfo and /mclear
# 1.3.0	- remade big parts of the script. Added a /mvolume
# 1.2.1	- changed my email address to the evilninja.org domain.
# 1.2	- added /mload, /mremove, /madd - thus breaking the no-playlist-thing. :)
# 1.1	- added /mstop, /mrepeat and /mrandom
############################################################################
use strict;

use Irssi;
use Irssi::Irc;
use Irssi::TextUI;
use Audio::MPD;
use Env qw(MPD_PORT MPD_HOST);

use vars qw($VERSION %IRSSI);

$VERSION = "2.1.2";

%IRSSI = (
				author		=> 'bumby',
				contact		=> 'bumbyn@gmail.com',
				name		=> 'impc',
				description	=> 'Irssi MPD control',
				license		=> 'BSD',
				changed		=> '2007/12/28'
			);

############################################################################
# Setups
#
our %MPD = ();
$MPD{'port'}	= $ENV{'MPD_PORT'}?$ENV{'MPD_PORT'}:"6600";
$MPD{'host'}	= $ENV{'MPD_HOST'}?$ENV{'MPD_HOST'}:"localhost";
$MPD{'timeout'}	= "5";

our $mpd = new Audio::MPD(hostname =>$MPD{'host'}, port => $MPD{'port'});

our %STATE = ();
$STATE{'play'}	= ' >';
$STATE{'pause'}	= '||';
$STATE{'stop'}	= '[]';

# to give impc a dedicated statusbar comment these two lines
#Irssi::command("statusbar window add mpdbar");
#Irssi::command("statusbar window enable mpdbar");
# and uncomment these
Irssi::command("statusbar dmpdbar placement top");
Irssi::command("statusbar dmpdbar position 0");
Irssi::command("statusbar dmpdbar enable");
Irssi::command("statusbar dmpdbar add mpdbar");
Irssi::command("statusbar dmpdbar visible active");

Irssi::settings_add_bool('misc', 'mpd_show_state', '1');
Irssi::settings_add_bool('misc', 'mpd_show_artist', '1');
Irssi::settings_add_bool('misc', 'mpd_show_album', '0');
Irssi::settings_add_bool('misc', 'mpd_show_title', '1');
Irssi::settings_add_bool('misc', 'mpd_show_time', '1');
Irssi::settings_add_bool('misc', 'mpd_show_percent', '1');
Irssi::settings_add_bool('misc', 'mpd_show_volume', '0');
Irssi::settings_add_bool('misc', 'mpd_show_mode', '0');
Irssi::settings_add_bool('misc', 'mpd_autoreconnect', '0');
############################################################################

############################################################################
# irssi-bound commands
# 
sub play {
	my ($args, $server, $witem) = @_;
	my @a = split(/ /, $args);
	my $type = lc shift @a;
	my $name = "";
	foreach my $element (@a){
		$name.=$element." ";
	}
	$name=~s/\s+$//g;

	if($type eq ""){
		eval { $mpd->play(0); };
		if($@) {
			$MPD{'state'}="Error: $@";
			Irssi::statusbar_items_redraw('mpdbar');
			return;
		}
		return;
	}

	if($name eq ""){$name=$type}

	$MPD{'state'}="Searching...";
	Irssi::statusbar_items_redraw('mpdbar');

	my @plylist;

	eval { @plylist = $mpd->playlist->as_items; };

	if($@) {
		$MPD{'state'}="Error: $@";
		Irssi::statusbar_items_redraw('mpdbar');
		return;
	}

	my $tracknr = undef;
	while( (my $track = shift @plylist) && not defined $tracknr) {
		if($track->{$type} =~ /$name/i) {
			eval { $mpd->play($track->{'pos'}); };
			if($@) {
				$MPD{'state'}="Error: $@";
				Irssi::statusbar_items_redraw('mpdbar');
			}
			return;
		}
	}
}

sub remove {
	my ($args, $server, $witem) = @_;
	my @a = split(/ /, $args);
	my $type = lc shift @a;
	my $name = "";
	foreach my $element (@a){
		$name.=$element." ";
	}
	$name=~s/\s+$//g;

	if($type eq ""){clear();return;}
	if($name eq ""){$name=$type}

	$MPD{'state'}="Searching...";
	Irssi::statusbar_items_redraw('mpdbar');

	my @plylist;

	eval { @plylist = $mpd->playlist->as_items; };

	if($@) {
		$MPD{'state'}="Error: $@";
		Irssi::statusbar_items_redraw('mpdbar');
		return;
	}

	my $tracknr = undef;
	while( (my $track = shift @plylist) && not defined $tracknr) {
		if($track->{$type} =~ /$name/i) {
			eval { $mpd->playlist->deleteid($track->{'id'}); };
			if($@) {
				$MPD{'state'}="Error: $@";
				Irssi::statusbar_items_redraw('mpdbar');
			}
		}
	}
}

sub add {
	my ($args, $server, $witem) = @_;
	my @a = split(/ /, $args);
	my $argc = $#a;
	my $type = shift @a;
	my $name = "";
	my @files;

	foreach my $element (@a){
		$name.=$element." ";
	}
	$name=~s/\s+$//g;

	if($name eq ""){$name=$type;$type='artist';}


	$MPD{'state'}="Searching...";
	Irssi::statusbar_items_redraw('mpdbar');

	if($argc < 0) {
		eval { @files = $mpd->collection->all_items_simple; };
		if($@) {
			$MPD{'state'}="Error: $@";
			Irssi::statusbar_items_redraw('mpdbar');
			return;
		}
	} else {
		if($type eq 'title') {
			eval { @files = $mpd->collection->songs_with_title_partial($name); };
		} elsif($type eq 'album') {
			eval { @files = $mpd->collection->songs_from_album_partial($name); };
		} else {
			eval { @files = $mpd->collection->songs_by_artist_partial($name); };
		}

		if($@) {
			$MPD{'state'}="Error: $@";
			Irssi::statusbar_items_redraw('mpdbar');
			return;
		}
	}

	$MPD{'state'}="Adding...";
	Irssi::statusbar_items_redraw('mpdbar');

	eval { foreach(@files){$mpd->playlist->add($_->{'file'}) if exists $_->{'file'}} };
	if($@) {
		$MPD{'state'}="Error: $@";
		Irssi::statusbar_items_redraw('mpdbar');
	}
}

sub load {
	my ($args, $server, $witem) = @_;

	clear();
	add($args, $server, $witem);
	eval { $mpd->play(0); };
	if($@) {
		$MPD{'state'}="Error: $@";
		Irssi::statusbar_items_redraw('mpdbar');
		return;
	}
}

sub seek  {
	my ($args, $server, $witem) = @_;
	if($args<0 and $args>100){return;}

	my $seektime = $MPD{'current'}->{'time'} * ($args/100);
	$mpd->seek($seektime);
}

sub volume {
	my ($args, $server, $witem) = @_;
	unless($args){
		Irssi::print("Current volume: ".$MPD{'status'}->{'volume'});
		return;
	}
	if($args<0 and $args>100){return;}
	$mpd->volume($args);
}

sub random {
	my ($args, $server, $witem) = @_;
	if($args=~/(1|on)/i){
		$mpd->random(1);
	}elsif($args=~/(0|off)/i){
		$mpd->random(0);
	}else{
		$mpd->random;
	}
}

sub repeat {
	my ($args, $server, $witem) = @_;
	if($args=~/(1|on)/i){
		$mpd->repeat(1);
	}elsif($args=~/(0|off)/i){
		$mpd->repeat(0);
	}else{
		$mpd->repeat;
	}
}

sub info  {
	Irssi::statusbar_items_redraw('mpdbar');
	if($MPD{'current'}->{'title'}){
		Irssi::active_win()->print(	$MPD{'current'}->{'title'});
	}else{
		Irssi::active_win()->print(	$MPD{'current'}->{'file'});
	}

	Irssi::active_win()->print(		$MPD{'state'}." #".$MPD{'status'}->{'song'}."/".$MPD{'status'}->{'playlistlength'}.
						"   ".$MPD{'status'}->{'time'}->{'sofar'}." (".$MPD{'status'}->{'time'}->{'percent'}."%)");
	my $repeat = $MPD{'status'}->{'repeat'}?"on":"off";
	my $random = $MPD{'status'}->{'random'}?"on":"off";
	Irssi::active_win()->print(		"volume: ".$MPD{'status'}->{'volume'}."%   repeat: ".$repeat.
						"   random: ".$random);
}
sub stop {
	eval { $mpd->stop; };
	if($@) {
		$MPD{'state'}="Error: $@";
		Irssi::statusbar_items_redraw('mpdbar');
		return;
	}
}

sub pause {
	eval { $mpd->pause; };
	if($@) {
		$MPD{'state'}="Error: $@";
		Irssi::statusbar_items_redraw('mpdbar');
	}
}
sub next {
	eval { $mpd->next; };
	if($@) {
		$MPD{'state'}="Error: $@";
		Irssi::statusbar_items_redraw('mpdbar');
	}
}
sub prev {
	eval { $mpd->prev; };
	if($@) {
		$MPD{'state'}="Error: $@";
		Irssi::statusbar_items_redraw('mpdbar');
	}
}
sub clear {
	eval { $mpd->playlist->clear; };
	if($@) {
		$MPD{'state'}="Error: $@";
		Irssi::statusbar_items_redraw('mpdbar');
	}
}

############################################################################
# Setup the statusbar
#
sub mpdbar_setup  {
	my ($item, $get_size_only) = @_;
	my $info = "";

	return unless $MPD{'status'};

	$info.=$MPD{'state'}." " if(Irssi::settings_get_bool('mpd_show_state'));
	if($MPD{'current'}->{'title'}) {
		$info.=$MPD{'current'}->{'artist'}." - " if(Irssi::settings_get_bool('mpd_show_artist'));
		$info.=$MPD{'current'}->{'album'}." - " if(Irssi::settings_get_bool('mpd_show_album'));
		$info.=$MPD{'current'}->{'title'}." " if(Irssi::settings_get_bool('mpd_show_title'));
		$info=~s/( - )+$//;
	} else {
		$info.=$MPD{'current'}->{'file'}." ";
	}

	$info.= "(";
	if(Irssi::settings_get_bool('mpd_show_time')) {
		$info.= $MPD{'status'}->{'time'}->{'sofar'}."/".$MPD{'status'}->{'time'}->{'total'};
		$info.= " ";
	}

	if(Irssi::settings_get_bool('mpd_show_percent')) {
		$info.= int($MPD{'status'}->{'time'}->{'percent'}+0.5)."%";
		$info.= " ";
	}

	if(Irssi::settings_get_bool('mpd_show_volume')) {
		$info.= "Volume: ".$MPD{'status'}->{'volume'}."%";
		$info.= " ";
	}

	if(Irssi::settings_get_bool('mpd_show_mode')) {
		my $mode = $MPD{'status'}->{'repeat'}?"r":"";
		$mode .= $MPD{'status'}->{'random'}?"z":"";
		$info.="[$mode]";
	}
	$info.= ")";
	$info=~s/\(\)$//;

	$item->default_handler($get_size_only, undef, $info, 1)
}

############################################################################
# Refresh the statusbar
#
sub mpdbar_refresh  {
	my $status;
	my $csong;

	eval { $status = $mpd->status; };
	if($@) {
		$MPD{'state'}="Error: $@";
		Irssi::statusbar_items_redraw('mpdbar');
		return;
	}

	eval { $csong = $mpd->current; };
	if($@) {
		$MPD{'state'}="Error: $@";
		Irssi::statusbar_items_redraw('mpdbar');
		return;
	}

	$MPD{'status'}	= $status;
	$MPD{'current'}	= $csong;
	$MPD{'state'}	= $STATE{$status->{'state'}} || "?";

	Irssi::statusbar_items_redraw('mpdbar');
}

############################################################################
# Print songtitle etc to active window.
sub show {
	my @actions=("np: ");
	my $t = $MPD{'current'}->{'title'};
	my $a = $MPD{'current'}->{'artist'};
	Irssi::active_win()->command("/me ".$actions[rand(@actions)]."$a - $t");
}

############################################################################
# Irssi-stuff
#
Irssi::statusbar_item_register('mpdbar', '{sb $0 $1 $2-}', 'mpdbar_setup');
Irssi::statusbars_recreate_items();
Irssi::timeout_add(1000, 'mpdbar_refresh', undef);

Irssi::command_bind('mshow','show');
Irssi::command_bind('mstop','stop');
Irssi::command_bind('mrepeat','repeat');
Irssi::command_bind('mrandom','random');
Irssi::command_bind('mplay','play');
Irssi::command_bind('mload','load');
Irssi::command_bind('madd','add');
Irssi::command_bind('mseek','seek');
Irssi::command_bind('mnext','next');
Irssi::command_bind('mprev','prev');
Irssi::command_bind('mpause','pause');
Irssi::command_bind('mvolume','volume');
Irssi::command_bind('mclear','clear');
Irssi::command_bind('minfo','info');
Irssi::command_bind('mremove','remove');
